# Manning better banking

```bash
./gradlew bootRun
```

Or

```bash
/gradlew build && java -jar build/libs/better-banking*.jar
```

Then

```bash
curl http://localhost:8080/transactions/12345
```

------
