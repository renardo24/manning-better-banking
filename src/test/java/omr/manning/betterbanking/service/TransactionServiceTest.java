package omr.manning.betterbanking.service;

import omr.manning.betterbanking.entity.Transaction;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public final class TransactionServiceTest {

    @Autowired
    private TransactionService transactionService;

    @Test
    public void should_have_several_transactions() {
        final Integer accountNumber = 1234567;
        List<Transaction> transactions = transactionService.findAllByAccountNumber(accountNumber);
        assertEquals(3, transactions.size());

    }
}
