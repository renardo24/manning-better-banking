package omr.manning.betterbanking.web;

import omr.manning.betterbanking.service.TransactionService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;

public final class TransactionComponentTest {

    @LocalServerPort
    private int port;

    @Test
    public void testApplicationEndToEnd() {
        given().standaloneSetup(new TransactionController(new TransactionService()))
               .when()
               .get(String.format("http://localhost:%s/transactions/%s", port, 1234567))
               .then()
               .statusCode(Matchers.is(200))
               .and()
               .contentType(MediaType.APPLICATION_JSON.getType());
    }
}
