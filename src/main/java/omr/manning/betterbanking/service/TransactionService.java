package omr.manning.betterbanking.service;

import omr.manning.betterbanking.entity.Transaction;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public final class TransactionService {
    public List<Transaction> findAllByAccountNumber(final Integer accountNumber) {
        List<Transaction> transactions = new ArrayList<>();
        Transaction t1 = new Transaction("credit", accountNumber, "USD", 100.00, "acme", "acme.png");
        transactions.add(t1);
        Transaction t2 = new Transaction("credit", accountNumber, "GBP", 10.00, "acme", "acme.png");
        transactions.add(t2);
        Transaction t3 = new Transaction("credit", accountNumber, "EUR", 20.00, "acme", "acme.png");
        transactions.add(t3);
        return transactions;
    }
}
